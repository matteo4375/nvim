{
  description = "My nvim config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      
      runtimeDeps = with pkgs; [
        nil
        lua-language-server
        clang-tools
        rust-analyzer
        zls
        typescript
        nodePackages.typescript-language-server

        git
        curl
        unzip
        gcc
        cargo
        rustc
        python3
        nodejs
      ];
    in {
      packages = rec {
        neovim = pkgs.wrapNeovimUnstable pkgs.neovim-unwrapped (pkgs.neovimUtils.makeNeovimConfig {
          customRC = ''
            set runtimepath^=${./.}
            source ${./.}/init.lua
          '';
          } // {
          wrapperArgs = [
            "--suffix"
            "PATH"
            ":"
            "${nixpkgs.lib.makeBinPath runtimeDeps}"
          ];
        });

        default = neovim;
      };
    }
  );
}
