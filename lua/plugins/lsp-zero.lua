return {
  {
    "VonHeikemen/lsp-zero.nvim",
    branch = "v2.x",
    dependencies = {
      --- Uncomment the two plugins below if you want to manage the language servers from neovim
      -- "williamboman/mason.nvim",
      -- "williamboman/mason-lspconfig.nvim",

      -- LSP Support
      "neovim/nvim-lspconfig",

      -- Autocompletion
      "hrsh7th/nvim-cmp",
      "hrsh7th/cmp-nvim-lsp",
      "L3MON4D3/LuaSnip",
      "onsails/lspkind.nvim",
    },
    config = function()
      local lsp = require "lsp-zero"

      lsp.preset "recommended"

      lsp.set_preferences {
        sign_icons = {
          error = "✘",
          warn = "▲",
          hint = "⚑",
          info = "»",
        },
      }

      lsp.setup_servers {
        "lua_ls",
        "rust_analyzer",
        "clangd",
        "zls",
        "tsserver",
      }

      require("lspconfig").nil_ls.setup {
        settings = {
          ["nil"] = {
            nix = {
              maxMemoryMB = 2048,
              flake = {
                autoArchive = true,
                autoEvalInputs = true,
              },
            },
          },
        },
      }

      local cmp = require "cmp"
      local cmp_select = {behavior = cmp.SelectBehavior.Select}
      local cmp_mappings = {
        ["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
        ["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
        ["<Tab>"] = cmp.mapping.confirm({select = true}),
      }

      lsp.setup_nvim_cmp({
        mapping = cmp_mappings
      })

      lsp.setup()

      lsp.on_attach(function(client, bufnr)
        -- see :help lsp-zero-keybindings
        -- to learn the available actions
        --lsp.default_keymaps({buffer = bufnr})
        local opts = {buffer = bufnr, remap = false}

        vim.keymap.set("n", "<leader>ca", function()
          vim.lsp.buf.code_action { apply = true }
        end, opts)
        vim.keymap.set("n", "<leader>cd", vim.lsp.buf.definition, opts)
        vim.keymap.set("n", "<leader>ck", vim.lsp.buf.hover, opts)
        vim.keymap.set("n", "<leader>crr", vim.lsp.buf.references, opts)
        vim.keymap.set("n", "<leader>crn", vim.lsp.buf.rename, opts)
      end)
    end,
  }
}
