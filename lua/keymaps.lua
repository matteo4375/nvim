function toggle_colorcolumn()
    local value = vim.inspect(vim.opt.colorcolumn:get())
    if value == "{}" then
        vim.opt.colorcolumn = { 80 }
    else
        vim.opt.colorcolumn = {}
    end
end

vim.g.mapleader = " "

vim.keymap.set("n", "<leader>w", "<C-w>")

vim.keymap.set("n", "<leader>pv", ":Ex<CR>")

vim.keymap.set("x", "J", ":move '>+1<CR>gv=gv")
vim.keymap.set("x", "K", ":move '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("x", "<leader>p", "\"_dP")

vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")

vim.keymap.set("n", "<leader>D", "\"_d")
vim.keymap.set("v", "<leader>D", "\"_d")

vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

vim.keymap.set("n", "th", "<cmd>:tabprev<CR>")
vim.keymap.set("n", "tl", "<cmd>:tabnext<CR>")
vim.keymap.set("n", "tn", "<cmd>:tabnew<CR>")
vim.keymap.set("n", "tc", "<cmd>:tabclose<CR>")

vim.keymap.set("n", "<leader>oc", toggle_colorcolumn)

-- Unset arrow keys
vim.cmd [[
    noremap <Left> <Nop>
    noremap <Right> <Nop>
    noremap <Up> <Nop>
    noremap <Down> <Nop>


    inoremap <Left> <Nop>
    inoremap <Right> <Nop>
    inoremap <Up> <Nop>
    inoremap <Down> <Nop>
]]

-- Disable Mouse
vim.cmd [[
    set mouse=
]]
